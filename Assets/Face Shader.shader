﻿Shader "Unlit/Face Shader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DisplaceTex ("DisplaceTexture", 2D) = "black" {}
		_DisplaceAmount ("DisplaceAmount", Float) = 0.1
		_AddTex1 ("TopTexture", 2D) = "black" {}
		_AddAmount1 ("TopAmount", Float) = 0.1
		_AddTex2 ("TopTexture", 2D) = "black" {}
		_AddAmount2 ("TopAmount", Float) = 0.1
		_MaskTexture ("MaskTexture", 2D) = "gray" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;

			sampler2D _DisplaceTex;
			float _DisplaceAmount;

			sampler2D _AddTex1;
			float _AddAmount1;

			sampler2D _AddTex2;
			float _AddAmount2;

			sampler2D _MaskTexture;

			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			fixed4 freeze (fixed4 input) {
				fixed4 output = fixed4(
					input.r * 0.5,
					input.g * input.r,
					input.b + input.r,
					input.a
				);
				return output;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float2 deform = i.uv + tex2D(_DisplaceTex, i.uv) * _DisplaceAmount;
				// sample the texture
				fixed4 col = clamp(freeze(tex2D(_MainTex, deform)), 0.2, 0.7) * 1.5;
				col *= tex2D(_MaskTexture, i.uv);
				col += tex2D(_AddTex1, i.uv) * _AddAmount1;
				col += tex2D(_AddTex2, i.uv) * _AddAmount2;
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
