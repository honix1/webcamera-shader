﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebCamera : MonoBehaviour {
   
    public Vector2Int resolution = new Vector2Int(1920, 1080);
    public int fps = 30;

    WebCamTexture webCamTexture;

    public WebCamTexture GetTexture() {
        return webCamTexture;
    }

    void Awake () {
        webCamTexture = new WebCamTexture(resolution.x, resolution.y, fps);
        webCamTexture.name = "WebCamTexture";
        webCamTexture.Play();
	}

    void Start() {
        StartCoroutine(CheckCamera());
    }

    IEnumerator CheckCamera() {
        while (true) {
            yield return new WaitForSeconds(3f);
            if (!webCamTexture.isPlaying) {
                Debug.LogWarning("Resetting camera");
                webCamTexture.Stop();
                webCamTexture.Play();
            }
        }
    }
}
