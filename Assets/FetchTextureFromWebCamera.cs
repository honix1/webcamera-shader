﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FetchTextureFromWebCamera : MonoBehaviour {

    public WebCamera webCameraHolder;
    
	void Start () {
        GetComponent<RawImage>().enabled = true;
        GetComponent<RawImage>().texture = webCameraHolder.GetTexture();
	}
}
